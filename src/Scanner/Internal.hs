module Scanner.Internal (Token (..), ScanError (..), scanTokens) where

import Control.Applicative (Alternative ((<|>)))
import Control.Monad.State (MonadState (state), State, evalState, runState)
import Data.Char (isAlpha, isAlphaNum, isDigit)
import Data.Either (lefts, rights)
import Data.Foldable (asum)
import Data.List (isPrefixOf)
import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)

data Token
  = LeftParen
  | RightParen
  | LeftBrace
  | RightBrace
  | Comma
  | Dot
  | Minus
  | Plus
  | Semicolon
  | Slash
  | Asterisk
  | Bang
  | BangEqual
  | Equal
  | EqualEqual
  | Greater
  | GreaterEqual
  | Less
  | LessEqual
  | Identifier String
  | StringLiteral String
  | Number Double
  | And
  | Class
  | Else
  | FalseToken
  | Fun
  | For
  | If
  | Nil
  | Or
  | Print
  | Return
  | Super
  | This
  | TrueToken
  | Var
  | While
  | Eof
  deriving (Eq, Show)

type Offset = Int

data ScanError
  = UnknownToken Offset Char
  | UnterminatedString Offset
  | {- InvalidLiteral start end, where the invalid literal is between indices [start, end).-}
    InvalidLiteral Offset Offset
  deriving (Eq, Show)

scanTokens :: String -> Either [ScanError] [Token]
scanTokens input =
  let results = scanTokens' $ zip [0 ..] input
      errors = lefts results
      tokens = rights results ++ [Eof]
   in case errors of
        [] -> Right tokens
        _ -> Left errors

scanTokens' :: [(Int, Char)] -> [Either ScanError Token]
scanTokens' [] = []
scanTokens' xs =
  let (token, rest) = runState scanToken xs
   in case token of
        TokenResult t -> Right t : scanTokens' rest
        NoTokenResult -> scanTokens' rest
        ErrorResult e -> Left e : scanTokens' rest

data ScanTokenResult = TokenResult Token | NoTokenResult | ErrorResult ScanError

-- Scans a single token,
--
-- The input list must be non-empty.
scanToken :: State [(Int,Char)] ScanTokenResult
scanToken = state $ \xs ->
  let attemptedScan = asum $ map (liftMaybe . (`runState` xs)) scanners
      unknownTokenError = ErrorResult . uncurry UnknownToken $ head xs
   in fromMaybe (unknownTokenError, tail xs) attemptedScan

liftMaybe :: (Maybe a, b) -> Maybe (a, b)
liftMaybe (Just x, y) = Just (x, y)
liftMaybe _ = Nothing

scanners :: [State [(Int, Char)] (Maybe ScanTokenResult)]
scanners =
  [ scanComment,
    scanWhitespace,
    scanNumber,
    scanConstToken "!=" BangEqual,
    scanConstToken "==" EqualEqual,
    scanConstToken "<=" LessEqual,
    scanConstToken ">=" GreaterEqual,
    scanConstToken "==" EqualEqual,
    scanConstToken "!" Bang,
    scanConstToken "=" Equal,
    scanConstToken "<" Less,
    scanConstToken ">" Greater,
    scanConstToken "(" LeftParen,
    scanConstToken ")" RightParen,
    scanConstToken "{" LeftBrace,
    scanConstToken "}" RightBrace,
    scanConstToken "," Comma,
    scanConstToken "." Dot,
    scanConstToken "-" Minus,
    scanConstToken "+" Plus,
    scanConstToken ";" Semicolon,
    scanConstToken "*" Asterisk,
    scanConstToken "/" Slash,
    scanConstToken "and" And,
    scanConstToken "class" Class,
    scanConstToken "else" Else,
    scanConstToken "false" FalseToken,
    scanConstToken "for" For,
    scanConstToken "if" If,
    scanConstToken "nil" Nil,
    scanConstToken "or" Or,
    scanConstToken "print" Print,
    scanConstToken "return" Return,
    scanConstToken "super" Super,
    scanConstToken "this" This,
    scanConstToken "true" TrueToken,
    scanConstToken "var" Var,
    scanConstToken "while" While,
    scanString,
    scanIdentifier
  ]

scanComment :: State [(Int, Char)] (Maybe ScanTokenResult)
scanComment = state $ \xs ->
  let string = map snd xs
   in if "//" `isPrefixOf` string
        then (Just NoTokenResult, dropUntil (\x -> snd x == '\n') xs)
        else (Nothing, xs)

scanWhitespace :: State [(Int, Char)] (Maybe ScanTokenResult)
scanWhitespace = state scanWhitespace'
scanWhitespace' [] = (Nothing, [])
scanWhitespace' xs =
  let isSpace (_, x) = x == ' ' || x == '\r' || x == '\n' || x == '\t'
      startsWithSpace = isSpace (head xs)
   in if startsWithSpace
        then (Just NoTokenResult, dropWhile isSpace xs)
        else (Nothing, xs)

dropUntil :: (a -> Bool) -> [a] -> [a]
dropUntil _ [] = []
dropUntil f (x : xs) = if f x then xs else dropUntil f xs

scanConstToken :: String -> Token -> State [(Int, Char)] (Maybe ScanTokenResult)
scanConstToken prefix t = state $ \xs ->
  let string = map snd xs
   in if prefix `isPrefixOf` string
        then (Just $ TokenResult t, drop (length prefix) xs)
        else (Nothing, xs)

scanString :: State [(Int, Char)] (Maybe ScanTokenResult)
scanString = state scanString'

scanString' :: [(Offset, Char)] -> (Maybe ScanTokenResult, [(Offset, Char)])
scanString' [] = (Nothing, [])
scanString' ((i, '"') : xs) =
  let (quoted, rest) = span (\x -> snd x /= '"') xs
      string = map snd quoted
      isTerminated = not . null $ rest
      parsedString = Just . TokenResult . StringLiteral $ string
      error = Just . ErrorResult . UnterminatedString $ i
   in if isTerminated then (parsedString, tail rest) else (error, [])
scanString' xs = (Nothing, xs)

scanNumber :: State [(Int, Char)] (Maybe ScanTokenResult)
scanNumber = state scanNumber'

scanNumber' :: [(Int, Char)] -> (Maybe ScanTokenResult, [(Int, Char)])
scanNumber' [] = (Nothing, [])
scanNumber' xs
  | null numberDescr = (Nothing, xs)
  -- If we matched just ".", then there were no numbers before/after the dot, so it's not part of a
  -- number. We match this to the token Dot later.
  | numberDescr == "." = (Nothing, xs)
  | isValidDescr numberDescr = (result, rest)
  | otherwise = (error, rest)
  where
    isDigitOrDot x = isDigit x || x == '.'
    isValidDescr descr =
      not (null descr)
        && head descr /= '.'
        && last descr /= '.'
        && count (== '.') descr <= 1

    numberDescr = takeWhile isDigitOrDot (map snd xs)
    scannedNumber = readMaybe numberDescr
    rest = drop (length numberDescr) xs

    literalStart = fst . head $ xs
    literalEnd = literalStart + length numberDescr
    error = Just . ErrorResult $ InvalidLiteral literalStart literalEnd
    result = fmap (TokenResult . Number) scannedNumber

count :: (a -> Bool) -> [a] -> Int
count _ [] = 0
count f (x : xs)
  | f x = 1 + count f xs
  | otherwise = count f xs

scanIdentifier :: State [(Int, Char)] (Maybe ScanTokenResult)
scanIdentifier = state scanIdentifier'

scanIdentifier' :: [(Int, Char)] -> (Maybe ScanTokenResult, [(Int, Char)])
scanIdentifier' [] = (Nothing, [])
scanIdentifier' xs
  | isAlpha (snd . head $ xs) =
      let identifier = takeWhile isAlphaNum (map snd xs)
          result = TokenResult . Identifier $ identifier
          rest = drop (length identifier) xs
       in (Just result, rest)
  | otherwise = (Nothing, xs)