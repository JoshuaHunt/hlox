{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TupleSections #-}

module Parser (Expression (..), parseExpression, ParseError (..)) where

import Control.Monad (unless, when)
import Control.Monad.State (MonadState (get, put), state)
import Data.Maybe (fromMaybe, isJust)
import Scanner (Token (..))
import Util (safeHead, maybeToRight)

data Expression
  = NumberExpression Double
  | StringExpression String
  | TrueExpression
  | FalseExpression
  | NilExpression
  | NegateExpression Expression
  | NotExpression Expression
  | EqualEqualExpression Expression Expression
  | BangEqualExpression Expression Expression
  | LessEqualExpression Expression Expression
  | LessExpression Expression Expression
  | GreaterEqualExpression Expression Expression
  | GreaterExpression Expression Expression
  | PlusExpression Expression Expression
  | MinusExpression Expression Expression
  | TimesExpression Expression Expression
  | DivideExpression Expression Expression
  {- TODO: Apparently we will need a GroupingExpression to correctly handle the LHS of assignment
  expressions
  -}
  deriving (Eq, Show)

type Offset = Int

{-
TODO: At some point, we should include the offset in the error message.
-}
newtype ParseError = ParseError String
  deriving (Eq, Show)

newtype Parse a = Parse
  { runParse :: [Token] -> Either ParseError (a, [Token])
  }

liftEither :: Either ParseError a -> Parse a
liftEither aOr = Parse $ \tokens -> (,tokens) <$> aOr

liftMaybe :: ParseError -> Maybe a -> Parse a
liftMaybe e aOr = Parse $ \tokens -> (,tokens) <$> maybeToRight e aOr

instance Functor Parse where
  fmap f p = pure f <*> p

mapFst :: (a -> c) -> (a, b) -> (c, b)
mapFst f (a, b) = (f a, b)

instance Applicative Parse where
  pure a = Parse $ \tokens -> Right (a, tokens)
  fParser <*> aParser = Parse $ \tokens -> do
    (f, tokens') <- runParse fParser tokens
    (a, tokens'') <- runParse aParser tokens'
    return (f a, tokens'')

instance Monad Parse where
  return = pure
  xParser >>= f = Parse $ \tokens -> do
    (x, tokens') <- runParse xParser tokens
    runParse (f x) tokens'

instance MonadFail Parse where
  fail s = Parse $ \tokens -> Left (ParseError s)

instance MonadState [Token] Parse where
  state f = Parse $ Right . f

instance Semigroup (Parse a) where
  aParser <> bParser = Parse $ \tokens ->
    let a = runParse aParser tokens
        b = runParse bParser tokens
     in a <> b

parseExpression :: [Token] -> Either ParseError Expression
parseExpression tokens = fst <$> runParse parseEquality tokens

-- TODO: handle multiple errors

parseEquality :: Parse Expression
parseEquality =
  parseLeftAssocBinary
    parseComparison
    [ (EqualEqual, EqualEqualExpression),
      (BangEqual, BangEqualExpression)
    ]

parseComparison :: Parse Expression
parseComparison =
  parseLeftAssocBinary
    parseTerm
    [ (Greater, GreaterExpression),
      (GreaterEqual, GreaterEqualExpression),
      (Less, LessExpression),
      (LessEqual, LessEqualExpression)
    ]

parseTerm :: Parse Expression
parseTerm = parseLeftAssocBinary parseFactor [(Plus, PlusExpression), (Minus, MinusExpression)]

parseFactor :: Parse Expression
parseFactor = parseLeftAssocBinary parseUnary [(Asterisk, TimesExpression), (Slash, DivideExpression)]

parseLeftAssocBinary :: Parse Expression -> [(Token, Expression -> Expression -> Expression)] -> Parse Expression
parseLeftAssocBinary parseTerm operatorLookup =
  -- The simple factors <> parseTerm implementation gives you right associativity. To get left
  -- associativity, we need to parse a maximal list of [term, operator, term, operator, ...] and
  -- then fold that.
  let parseAnotherTerm = do
        operator <- matchToken operatorLookup
        rhs <- parseTerm
        return (operator, rhs)
      parseTail :: Parse [(Expression -> Expression -> Expression, Expression)]
      parseTail = (parseAnotherTerm >>= \x -> (x :) <$> parseTail) <> return []
      applyOperator lhs (operator, rhs) = operator lhs rhs
   in do
        first <- parseTerm
        tailTerms <- parseTail
        return $ foldl applyOperator first tailTerms

parseUnary :: Parse Expression
parseUnary =
  let unary = do
        operator <-
          matchToken [(Bang, NotExpression), (Minus, NegateExpression)]
        operator <$> parseUnary
   in unary <> parsePrimary

matchToken :: [(Token, a)] -> Parse a
matchToken tokenMap = do
  (x : xs) <- get
  let success = lookup x tokenMap
      noMatchError = 
        ParseError $
          "Couldn't match token, expected one of " ++ (show . keys $ tokenMap)
          ++ " but got " ++ show x
  when (isJust success) (put xs)
  liftMaybe noMatchError success

keys :: [(k, v)] -> [k]
keys = map fst

parsePrimary :: Parse Expression
parsePrimary = do
  (x : xs) <- get
  put xs
  if x == LeftParen
    then do
      expr <- parseEquality
      expect RightParen
      return expr
    else liftEither $ parsePrimary' x

parsePrimary' :: Token -> Either ParseError Expression
parsePrimary' FalseToken = Right FalseExpression
parsePrimary' TrueToken = Right TrueExpression
parsePrimary' Nil = Right NilExpression
parsePrimary' (StringLiteral s) = Right $ StringExpression s
parsePrimary' (Number x) = Right $ NumberExpression x
parsePrimary' unmatchedToken = Left . ParseError $ "Unparsable token: " ++ show unmatchedToken

expect :: Token -> Parse ()
expect t = do
  xs <- get
  let error = ParseError $ "Expected token " ++ show t ++ " but reached end of input."
  let result =
        if
            | null xs -> Left error
            | head xs == t -> Right ()
            | otherwise -> Left error
  unless (null xs) (put $ tail xs)
  liftEither result
