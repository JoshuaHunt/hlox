module Evaluator where

import qualified Data.Ord as Ord
import Parser (Expression (BangEqualExpression, EqualEqualExpression, FalseExpression, GreaterEqualExpression, GreaterExpression, LessEqualExpression, LessExpression, NegateExpression, NilExpression, NotExpression, NumberExpression, StringExpression, TrueExpression, PlusExpression, MinusExpression, TimesExpression, DivideExpression))
import Util (maybeToRight)

data Value = NilValue | BooleanValue Bool | NumberValue Double | StringValue String
  deriving (Eq, Show)

compareEq :: Value -> Value -> Bool
compareEq NilValue NilValue = True
compareEq (BooleanValue x) (BooleanValue y) = x == y
compareEq (StringValue x) (StringValue y) = x == y
compareEq (NumberValue x) (NumberValue y) = x == y
compareEq _ _ = False

-- Nothing indicates that the values cannot be ordered
compareLtEq :: Value -> Value -> Maybe Bool
compareLtEq (NumberValue x) (NumberValue y) = Just $ x <= y
compareLtEq _ _ = Nothing

type RuntimeError = String

evaluate :: Expression -> Either RuntimeError Value
evaluate (NumberExpression x) = Right $ NumberValue x
evaluate (StringExpression x) = Right $ StringValue x
evaluate TrueExpression = Right $ BooleanValue True
evaluate FalseExpression = Right $ BooleanValue False
evaluate NilExpression = Right NilValue
evaluate (NegateExpression x) = do
  inner <- evaluate x
  case inner of
    NumberValue x -> Right $ NumberValue (-x)
    x -> Left $ "Can't negate value " ++ show x
evaluate (NotExpression x) = fmap (BooleanValue . not . isTruthy) (evaluate x)
evaluate (EqualEqualExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  return . BooleanValue $ compareEq l r
evaluate (BangEqualExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  return . BooleanValue . not $ compareEq l r
evaluate (LessEqualExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  let error = noComparisonError l "<=" r
  ltEq <- maybeToRight error $ compareLtEq l r
  return . BooleanValue $ ltEq
evaluate (LessExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  let error = noComparisonError l "<" r
  lt <- maybeToRight error . fmap not $ compareLtEq r l
  return . BooleanValue $ lt
evaluate (GreaterEqualExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  let error = noComparisonError l ">=" r
  gtEq <- maybeToRight error $ compareLtEq r l
  return . BooleanValue $ gtEq
evaluate (GreaterExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  let error = noComparisonError l ">" r
  gt <- maybeToRight error . fmap not $ compareLtEq l r
  return . BooleanValue $ gt
evaluate (MinusExpression lhs rhs) = applyNumericOperator (-) lhs rhs 
evaluate (TimesExpression lhs rhs) = applyNumericOperator (*) lhs rhs
evaluate (DivideExpression lhs rhs) = applyNumericOperator (/) lhs rhs
evaluate (PlusExpression lhs rhs) = do
  l <- evaluate lhs
  r <- evaluate rhs
  addValues l r

noComparisonError :: Value -> String -> Value -> String
noComparisonError l op r =
  "Can't compare '" ++ prettyShow l ++ "' to '" ++ prettyShow r ++ "' using operator '" ++ op ++ "'."

prettyShow :: Value -> String
prettyShow (StringValue x) = x
prettyShow (NumberValue x) = show x
prettyShow (BooleanValue x) = show x
prettyShow NilValue = "nil"

applyNumericOperator :: (Double -> Double -> Double) -> Expression -> Expression -> Either RuntimeError Value
applyNumericOperator op lhs rhs = do
  l <- evaluate lhs
  r <- evaluate rhs
  applyNumericOperator' op l r

applyNumericOperator' :: (Double -> Double -> Double) -> Value -> Value -> Either RuntimeError Value
applyNumericOperator' op (NumberValue x) (NumberValue y) = Right $ NumberValue (x `op` y)
applyNumericOperator' _ x y = Left $ "Operator expected numbers but got " ++ show x ++ " and " ++ show y

addValues :: Value -> Value -> Either RuntimeError Value
addValues (StringValue x) (StringValue y) = Right $ StringValue (x ++ y)
addValues (NumberValue x) (NumberValue y) = Right $ NumberValue (x + y)
addValues x y = Left $ "Operator + cannot be applied to values " ++ show x ++ " and " ++ show y

isTruthy :: Value -> Bool
isTruthy NilValue = False
isTruthy (BooleanValue x) = x
isTruthy _ = True