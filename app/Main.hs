module Main where

import System.IO
import System.Exit (exitWith, ExitCode(ExitFailure))
import Data.Bifoldable (bifoldMap)
import Options.Applicative (
    (<**>),
    (<|>),
    argument,
    execParser,
    flag',
    fullDesc,
    header,
    help,
    helper,
    info,
    long,
    metavar,
    Parser,
    progDesc,
    str)
import Control.Monad (forever)
import Scanner (scanTokens, ScanError, Token)

data Flags = FileFlags FilePath | StdinFlags

fileParser :: Parser Flags
fileParser = FileFlags
            <$> argument str (metavar "SCRIPT" <> help "Script to execute.")

stdinParser :: Parser Flags
stdinParser = flag' StdinFlags (long "stdin" <> help "Read script from stdin")

flagsParser :: Parser Flags
flagsParser = fileParser <|> stdinParser

main :: IO ()
main = run =<< execParser opts
    where
        opts = info (flagsParser <**> helper)
            ( fullDesc
            <> progDesc "Run an hlox script."
            <> header "hlox - a compiler for Lox, written in Haskell")

run :: Flags -> IO ()
run (FileFlags scriptPath) = do
    handle <- openFile scriptPath ReadMode
    contents <- hGetContents handle
    let handleError e = logError e >> exitWith (ExitFailure 65)
    bifoldMap handleError runLine (parseLine contents)

run StdinFlags = forever $ do
    putStr "> "
    hFlush stdout
    line <- getLine
    bifoldMap logError runLine (parseLine line)


type ParseError = [ScanError]
type ParsedCode = [Token]
parseLine :: String -> Either ParseError ParsedCode
parseLine = scanTokens

runLine :: ParsedCode -> IO ()
-- TODO: make this do something interesting
runLine = putStrLn . unlines . map show

logError :: ParseError -> IO ()
logError errors = let describeError = ("Error: "++) . show
                      errorDescriptions = map describeError errors
                  in hPutStrLn stderr (unlines errorDescriptions)