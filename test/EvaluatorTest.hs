module EvaluatorTest (tests, properties) where

import Evaluator
import Parser (Expression (..))
import Test.Framework.Providers.HUnit (testCase)
import Test.HUnit (Assertion, assertEqual, assertFailure)
import Test.QuickCheck (Property, quickCheck, (==>))

prop_stringLiteral :: String -> Bool
prop_stringLiteral x =
  evaluate (StringExpression x) == Right (StringValue x)

prop_negate :: Double -> Bool
prop_negate x =
  evaluate (NegateExpression (NumberExpression x))
    == Right (NumberValue (-x))

evaluateNotExpresion =
  testCase "Evaluate logical negation" $
    return ()
      <> assertEqual
        "Number is true"
        (Right $ BooleanValue False)
        (evaluate $ NotExpression (NumberExpression 3))
      <> assertEqual
        "Nil is false"
        (Right $ BooleanValue True)
        (evaluate $ NotExpression NilExpression)
      <> assertEqual
        "False is false"
        (Right $ BooleanValue True)
        (evaluate $ NotExpression FalseExpression)
      <> assertEqual
        "True is true"
        (Right $ BooleanValue False)
        (evaluate $ NotExpression TrueExpression)
      <> assertEqual
        "String is true"
        (Right $ BooleanValue False)
        (evaluate $ NotExpression (StringExpression "hi"))

prop_minus :: Double -> Double -> Bool
prop_minus x y =
  evaluate (MinusExpression (NumberExpression x) (NumberExpression y))
    == Right (NumberValue (x - y))

prop_div :: Double -> Double -> Property
prop_div x y =
  y /= 0 ==> prop_div' x y
  where
    prop_div' x y =
      evaluate (DivideExpression (NumberExpression x) (NumberExpression y))
        == Right (NumberValue (x / y))

prop_mult :: Double -> Double -> Bool
prop_mult x y =
  evaluate (TimesExpression (NumberExpression x) (NumberExpression y))
    == Right (NumberValue (x * y))

prop_add :: Double -> Double -> Bool
prop_add x y =
  evaluate (PlusExpression (NumberExpression x) (NumberExpression y))
    == Right (NumberValue (x + y))

prop_concat :: String -> String -> Bool
prop_concat x y =
  evaluate (PlusExpression (StringExpression x) (StringExpression y))
    == Right (StringValue (x ++ y))

prop_less :: Double -> Double -> Bool
prop_less x y =
  evaluate (LessExpression (NumberExpression x) (NumberExpression y))
    == Right (BooleanValue (x < y))

prop_lessEq :: Double -> Double -> Bool
prop_lessEq x y =
  evaluate (LessEqualExpression (NumberExpression x) (NumberExpression y))
    == Right (BooleanValue (x <= y))

prop_noStringLtEq :: String -> String -> Bool
prop_noStringLtEq x y =
  evaluate (LessEqualExpression (StringExpression x) (StringExpression y))
    == Left ("Can't compare '" ++ x ++ "' to '" ++ y ++ "' using operator '<='.")

prop_noStringLt :: String -> String -> Bool
prop_noStringLt x y =
  evaluate (LessExpression (StringExpression x) (StringExpression y))
    == Left ("Can't compare '" ++ x ++ "' to '" ++ y ++ "' using operator '<'.")

prop_noStringGtEq :: String -> String -> Bool
prop_noStringGtEq x y =
  evaluate (GreaterEqualExpression (StringExpression x) (StringExpression y))
    == Left ("Can't compare '" ++ x ++ "' to '" ++ y ++ "' using operator '>='.")

prop_noStringGt :: String -> String -> Bool
prop_noStringGt x y =
  evaluate (GreaterExpression (StringExpression x) (StringExpression y))
    == Left ("Can't compare '" ++ x ++ "' to '" ++ y ++ "' using operator '>'.")

prop_noStringArithmetic :: String -> String -> Bool
prop_noStringArithmetic x y =
  evaluate (DivideExpression (StringExpression x) (StringExpression y))
    == Left ("Operator expected numbers but got " ++ (show . StringValue $ x) ++ " and " ++ (show . StringValue $ y))

prop_eqStringString :: String -> String -> Bool
prop_eqStringString x y =
  evaluate (EqualEqualExpression (StringExpression x) (StringExpression y))
    == Right (BooleanValue (x == y))
    && evaluate (BangEqualExpression (StringExpression x) (StringExpression y))
      == Right (BooleanValue (x /= y))

prop_eqStringDouble :: String -> Double -> Bool
prop_eqStringDouble x y =
  evaluate (EqualEqualExpression (StringExpression x) (NumberExpression y))
    == Right (BooleanValue False)
    && evaluate (BangEqualExpression (StringExpression x) (NumberExpression y))
      == Right (BooleanValue True)
    && evaluate (EqualEqualExpression (NumberExpression y) (StringExpression x))
      == Right (BooleanValue False)
    && evaluate (BangEqualExpression (NumberExpression y) (StringExpression x))
      == Right (BooleanValue True)

prop_eqDoubleDouble :: Double -> Double -> Bool
prop_eqDoubleDouble x y =
  evaluate (EqualEqualExpression (NumberExpression x) (NumberExpression y))
    == Right (BooleanValue (x == y))
    && evaluate (BangEqualExpression (NumberExpression x) (NumberExpression y))
      == Right (BooleanValue (x /= y))

tests = [evaluateNotExpresion]

propTest name prop = putStrLn name >> quickCheck prop

properties =
  [ propTest "prop_minus" prop_minus,
    propTest "prop_div" prop_div,
    propTest "prop_mult" prop_mult,
    propTest "prop_add" prop_add,
    propTest "prop_concat" prop_concat,
    propTest "prop_less" prop_less,
    propTest "prop_lessEq" prop_lessEq,
    propTest "prop_negate" prop_negate,
    propTest "prop_stringLiteral" prop_stringLiteral,
    propTest "prop_eqStringString" prop_eqStringString,
    propTest "prop_eqStringDouble" prop_eqStringDouble,
    propTest "prop_eqDoubleDouble" prop_eqDoubleDouble,
    propTest "prop_noStringLtEq" prop_noStringLtEq,
    propTest "prop_noStringGtEq" prop_noStringGtEq,
    propTest "prop_noStringLt" prop_noStringLt,
    propTest "prop_noStringGt" prop_noStringGt,
    propTest "prop_noStringArithmetic" prop_noStringArithmetic
  ]