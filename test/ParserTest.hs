module ParserTest (tests) where

import Parser
import Scanner (Token (..))
import Test.Framework.Providers.HUnit (testCase)
import Test.HUnit (Assertion, assertEqual)

parseTrueToken =
  assertEqual
    "TrueToken becomes TrueExpression"
    (Right TrueExpression)
    (parseExpression [TrueToken])

reportsParseErrors =
  assertEqual
    "Reports unparsable tokens"
    (Left $ ParseError "Unparsable token: Semicolon")
    (parseExpression [Semicolon])

parsesUnaryOperators =
  assertEqual
    "Parse minus"
    (Right $ NegateExpression (NumberExpression 0.123))
    (parseExpression [Minus, Number 0.123])

parsesChainedUnaryOperators =
  assertEqual
    "Parse -!"
    (Right $ NegateExpression (NotExpression TrueExpression))
    (parseExpression [Minus, Bang, TrueToken])

parsesSimpleFactorExpression =
  assertEqual
    "Parses simple factor expression"
    (Right $ DivideExpression (NumberExpression 1.0) (NegateExpression (NumberExpression 2.0)))
    (parseExpression [Number 1.0, Slash, Minus, Number 2.0])

multiplicationLeftAssociative =
  assertEqual
    "Multiplication is left-associative"
    ( Right $
        TimesExpression
          ( TimesExpression
              (NumberExpression 1.0)
              (NumberExpression 2.0)
          )
          (NumberExpression 3.0)
    )
    (parseExpression [Number 1.0, Asterisk, Number 2.0, Asterisk, Number 3.0])

subtractionLeftAssociative =
  assertEqual
    "Subtraction is left-associative"
    ( Right $
        MinusExpression
          ( MinusExpression
              (NumberExpression 1.0)
              (NumberExpression 2.0)
          )
          (NumberExpression 3.0)
    )
    (parseExpression [Number 1.0, Minus, Number 2.0, Minus, Number 3.0])

multiplicationHigherPriorityThanAddition =
  assertEqual
    "Multiplication has higher priority than addition"
    ( Right $
        PlusExpression
          (NumberExpression 1.0)
          ( TimesExpression
              (NumberExpression 2.0)
              (NumberExpression 3.0)
          )
    )
    (parseExpression [Number 1.0, Plus, Number 2.0, Asterisk, Number 3.0])

parsesGreaterThan =
  assertEqual
    "Parses greater than"
    ( Right $
        GreaterExpression
          ( TimesExpression
              (NumberExpression 1.0)
              (NumberExpression 5.0)
          )
          ( PlusExpression
              (NumberExpression 2.0)
              (NumberExpression 3.0)
          )
    )
    (parseExpression [Number 1.0, Asterisk, Number 5.0, Greater, Number 2.0, Plus, Number 3.0])

parsesEquality =
  assertEqual
    "Parses equality"
    ( Right $
        EqualEqualExpression
          TrueExpression
          (LessExpression (NumberExpression 3.0) (NumberExpression 10.0))
    )
    (parseExpression [TrueToken, EqualEqual, Number 3.0, Less, Number 10.0])

parenthesesGroupTerms =
  assertEqual
    "Parentheses group terms"
    ( Right $
        TimesExpression
          (PlusExpression (NumberExpression 1.0) (NumberExpression 2.0))
          (NumberExpression 3.0)
    )
    (parseExpression [LeftParen, Number 1.0, Plus, Number 2.0, RightParen, Asterisk, Number 3.0])

unmatchedParenthesisGivesError =
  assertEqual
    "Expected unmatched parenthesis"
    (Left $ ParseError "Expected token RightParen but reached end of input.")
    (parseExpression [LeftParen, Number 1.0])

-- TODO: end-to-end test that connects scanning code with parsing code

tests =
  [ testCase "Parses token True" parseTrueToken,
    testCase "Reports unparsable token" reportsParseErrors,
    testCase "Parses minus" parsesUnaryOperators,
    testCase "Parses sequential unary operators" parsesChainedUnaryOperators,
    testCase "Parses simple factor expression" parsesSimpleFactorExpression,
    testCase "Multiplication is left-associative" multiplicationLeftAssociative,
    testCase "Subtraction is left-associative" subtractionLeftAssociative,
    testCase
      "Multiplication has higher priority than addition"
      multiplicationHigherPriorityThanAddition,
    testCase "Parses greater than" parsesGreaterThan,
    testCase "Parses equality" parsesEquality,
    testCase "Parentheses group terms" parenthesesGroupTerms,
    testCase "Unmatched parentheses" unmatchedParenthesisGivesError
  ]