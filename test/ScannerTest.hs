module ScannerTest (tests) where

import Scanner.Internal
import Test.Framework.Providers.HUnit (testCase)
import Test.HUnit (Assertion, assertEqual)

scanSingleTokens =
  assertEqual
    "Did not parse (.) correctly"
    (Right [LeftParen, Dot, RightParen, Eof])
    (scanTokens "(.)")

errorOnInvalidToken =
  assertEqual
    "Scanned invalid token"
    (Left [UnknownToken 1 '@'])
    (scanTokens "{@,.}")

scanDoubleTokens =
  assertEqual
    "Did not parse !=!<=> correctly"
    (Right [BangEqual, Bang, LessEqual, Greater, Eof])
    (scanTokens "!=!<=>")

ignoresComments =
  assertEqual
    "Ignores comments"
    (Right [GreaterEqual, Greater, Slash, Less, Eof])
    (scanTokens ">=///abcd!=foo\n>/<")

handlesCommmentsAtEof =
  assertEqual
    "Handles comments at the end of the file"
    (Right [EqualEqual, Eof])
    (scanTokens "==//")

invalidCharIndexIncludesComments =
  assertEqual
    "Comments count towards the offset of error messages"
    (Left [UnknownToken 3 '@'])
    (scanTokens "//\n@")

ignoresWhitespace =
  assertEqual
    "Ignore whitespace"
    (Right [Minus, Less, Bang, EqualEqual, Plus, Eof])
    (scanTokens "- <\n!\r==\t+")

handlesStrings =
  assertEqual
    "Handles strings"
    (Right [StringLiteral "foo", BangEqual, StringLiteral "bah", Plus, StringLiteral "baz", Eof])
    (scanTokens "\"foo\" != \"bah\" + \"baz\"")

errorOnUnterminatedString =
  assertEqual
    "Error on unterminated string"
    (Left [UnterminatedString 2])
    (scanTokens "!<\">+")

multipleErrorsReported =
  assertEqual
    "All scan errors are reported"
    (Left [UnknownToken 0 '@', UnknownToken 2 '¬', UnterminatedString 4])
    (scanTokens "@.¬,\"*+")

scanIntegerLiteral =
  assertEqual
    "Scans integer literal"
    (Right [Number 11.0, EqualEqual, Minus, Number 3.0, Eof])
    (scanTokens "11 == -3")

scanFloatingPoints =
  assertEqual
    "Scans floating point literals"
    (Right [StringLiteral "pi", EqualEqual, Number 3.14159, Eof])
    (scanTokens "\"pi\" == 3.14159")

leadingTrailingPointsDisallowed =
  assertEqual
    "Errors on trailing or leading decimal points"
    (Left [InvalidLiteral 0 4, InvalidLiteral 8 12])
    (scanTokens ".123 == 456.")

twoDecimalPointsInvalid =
  assertEqual
    "Errors on floating points with two decimal points"
    (Left [InvalidLiteral 0 7])
    (scanTokens "12.45.7")

scansKeyword =
  assertEqual
    "Scans a keyword"
    (Right [FalseToken, BangEqual, TrueToken, Eof])
    (scanTokens "false != true")

scansIdentifier =
  assertEqual
    "Scans an identifier"
    (Right [Var, Identifier "pi", Equal, Number 3.14, Eof])
    (scanTokens "var pi = 3.14")

-- TODO: error on identifiers like "0bad"? Currently scanned as [Number 0, Identifier "bad"].

tests =
  [ testCase "Scan single tokens" scanSingleTokens,
    testCase "Error on invalid tokens" errorOnInvalidToken,
    testCase "Scan double tokens" scanDoubleTokens,
    testCase "Ignore comments" ignoresComments,
    testCase "Handles comments at the end of the file" handlesCommmentsAtEof,
    testCase "Includes comments in error message offsets" invalidCharIndexIncludesComments,
    testCase "Ignores whitespace" ignoresWhitespace,
    testCase "Handles string literals" handlesStrings,
    testCase "Error on unterminated string" errorOnUnterminatedString,
    testCase "All scan errors are reported" multipleErrorsReported,
    testCase "Scan integer literal" scanIntegerLiteral,
    testCase "Scan floating point literal" scanFloatingPoints,
    testCase "Errors on trailing or leading decimal points" leadingTrailingPointsDisallowed,
    testCase "Errors on floating points with two decimal points" twoDecimalPointsInvalid,
    testCase "Scans a keyword" scansKeyword,
    testCase "Scans an identifier" scansIdentifier
  ]