module Main where

import qualified EvaluatorTest
import qualified ParserTest
import qualified ScannerTest
import Test.Framework (defaultMain)

main :: IO ()
main = do
    sequence_ EvaluatorTest.properties
    defaultMain (ScannerTest.tests ++ ParserTest.tests ++ EvaluatorTest.tests)